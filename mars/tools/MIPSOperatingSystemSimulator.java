package mars.tools;

import mars.Globals;
import mars.mips.hardware.AccessNotice;
import mars.mips.hardware.Memory;
import mars.mips.hardware.MemoryAccessNotice;
import mars.mips.moss.filemanager.FileManagerController;
import mars.mips.moss.memorymanager.MemoryController;
import mars.mips.moss.memorymanager.MemoryManager;
import mars.mips.moss.processmanager.ProcessController;
import mars.mips.moss.processmanager.ProcessTable;
import mars.util.SystemIO;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Vector;

import javax.swing.*;
import javax.swing.JToggleButton.ToggleButtonModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class MIPSOperatingSystemSimulator extends AbstractMarsToolAndApplication {
	private static String name    = "MOSS";
	private static String heading =  "M.I.P.S Operating System Simulator";
	private static String version = "Version 0.2";
	
	private static ProcessController processManager = new ProcessController();
	private static MemoryController memoryManager = new MemoryController();
	private static FileManagerController fileManager = new FileManagerController();	
	
	/**---------------------------------------------Select Options components------------------------------------------**/	
	private int op = 0;
	private JCheckBox JckOpProcessManager;
	private JCheckBox JckOpMemoryManager;
	private JCheckBox JckOpFileManager;
	

	public MIPSOperatingSystemSimulator(String title, String heading) {
		super(title, heading);
	}

    /**
     * Simple construction, likely used by the MARS Tools menu mechanism.
     */
    public MIPSOperatingSystemSimulator() {
    	super(name + ", " + version, heading);
    }
    public static void main(String[] args) {
        new MIPSOperatingSystemSimulator(heading+", "+version,heading).go();
    }
    
    @Override
	public String getName() {
		return name;
	}
    
    public void action(){
    	if(op == 0){
    		JDialog dialog = new JDialog(Globals.getGui(), this.getTitle());
            theWindow = dialog;
            
            JPanel contentPane = new JPanel(new BorderLayout(5,5));
            contentPane.setOpaque(true);
            contentPane.add(buildHeadingArea(), BorderLayout.NORTH);
            contentPane.add(buildSelectArea(),BorderLayout.CENTER);

            dialog.setContentPane(contentPane);
            dialog.pack();      
            dialog.setLocationRelativeTo(Globals.getGui());
            dialog.setVisible(true);
    	}else{
    		super.action();
    	}
        
    }
    
    protected JComponent buildSelectArea(){
    	Box proc = Box.createHorizontalBox();
		TitledBorder tc =new TitledBorder("Manager selector");
        tc.setTitleJustification(TitledBorder.CENTER);
        proc.setBorder(tc);
    	
    	JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.gridheight = c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(0, 0, 15, 15);		
		
		
		JckOpProcessManager = new JCheckBox("Process Manager", true);
		JckOpProcessManager.setEnabled(false);
		panel.add(JckOpProcessManager,c);
		
		c.gridx++;
		JckOpMemoryManager = new JCheckBox("Memory Manager", false);
		panel.add(JckOpMemoryManager,c);
		
		c.gridx++;
		JckOpFileManager = new JCheckBox("File Manager", false);
		panel.add(JckOpFileManager,c);		
		
		proc.add(panel);
		
		
		JPanel pan = new JPanel(new BorderLayout(10,5));
		
		pan.add(proc, BorderLayout.NORTH);
		
		Box proc2 = Box.createHorizontalBox();
		TitledBorder tc2 =new TitledBorder("Tool control");
        tc2.setTitleJustification(TitledBorder.CENTER);
        proc2.setBorder(tc2);
        
        JPanel pan2 = new JPanel(new BorderLayout(10,10));
		
		JButton btnChoose = new JButton("Select");
		btnChoose.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				  selectedOption();
				  } 
				} );
		btnChoose.setMaximumSize(new Dimension(60, 20));
		pan2.add(btnChoose,BorderLayout.EAST);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				  close();
				  } 
				} );
		btnCancel.setMaximumSize(new Dimension(60, 20));
		pan2.add(btnCancel,BorderLayout.WEST);
		
		proc2.add(pan2);
		
		pan.add(proc2, BorderLayout.CENTER);		
		
		return pan;
    }
    protected void close(){
    	this.theWindow.dispose();
    }
    
    protected void selectedOption(){
    	if(op == 0){
			if(JckOpProcessManager.isSelected())
				op +=1;
			if(JckOpMemoryManager.isSelected())
				op +=1;
			if(JckOpFileManager.isSelected())
				op +=2;
			//close window 
			this.theWindow.dispose();
			//open new window
			this.action();
		}
    }
    
	@Override
	protected JComponent buildMainDisplayArea() {
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.gridheight = c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(0, 0, 15, 15);
		
		JPanel pan = new JPanel(new BorderLayout());
		switch (op) {
		case 1:		// Process Manager only
			pan.add(processManager.buildMainDisplayArea(), BorderLayout.NORTH);
			
			panel.add(pan,c);
			break;
		case 2:		// Process Manager + Memory manager
			pan.add(processManager.buildMainDisplayArea(), BorderLayout.NORTH);
			pan.add( memoryManager.buildMainDisplayArea(), BorderLayout.CENTER);
			
			panel.add(pan,c);
			break;
		case 3:		// Process Manager + file manager
			pan.add(processManager.buildMainDisplayArea(), BorderLayout.NORTH);
			pan.add(fileManager.buildMainDisplayArea(),BorderLayout.CENTER);
			
			panel.add(pan,c);
			break;
		case 4:		// Process Manager + memory manager + file manager
			pan.add(processManager.buildMainDisplayArea(), BorderLayout.NORTH);
			pan.add(memoryManager.buildMainDisplayArea(), BorderLayout.CENTER);
			pan.add(fileManager.buildMainDisplayArea(),BorderLayout.SOUTH);
			
			panel.add(pan,c);
			break;
			
		default:
			break;
		}
		
		return panel;
	}
	
	@Override
	protected void addAsObserver() {
		addAsObserver(Memory.textBaseAddress, Memory.textLimitAddress);				
	}
	
	@Override
	public void processMIPSUpdate(Observable memory, AccessNotice notice) {
		if (notice.getAccessType() != AccessNotice.READ) return;
		MemoryAccessNotice m = (MemoryAccessNotice) notice;
		int a = m.getAddress();		
		processManager.processMIPSUpdate(memory, notice, a);
		if(op == 2 || op == 4)
			memoryManager.processMIPSUpdate(memory, notice, a);
		if(op == 3 || op == 4)
			fileManager.processMIPSUpdate(memory, notice, a);				
	}
	
	@Override
	protected void initializePreGUI() {		
		processManager.initializePreGUI();
		if(op == 2 || op == 4)
			memoryManager.initializePreGUI();
		if(op == 3 || op == 4)
			fileManager.initializePreGUI();
	}
	
	@Override
	protected void initializePostGUI() {
		processManager.initializePostGUI();
		if(op == 2 || op == 4)
			memoryManager.initializePostGUI();
		if(op == 3 || op == 4)
			fileManager.initializePostGUI();
    }
	
	@Override
	protected void reset() {				
		updateDisplay();
		processManager.reset();
		if(op == 2 || op == 4)
			memoryManager.reset();
		if(op == 3 || op == 4)
			fileManager.reset();
	}
  	@Override
	protected void updateDisplay() {		
		processManager.updateDisplay();
		if(op == 2 || op == 4)
			memoryManager.updateDisplay();
		if(op == 3 || op == 4)
			fileManager.updateDisplay();
	}
}

