package mars.mips.instructions.syscalls;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mars.ProcessingException;
import mars.ProgramStatement;
import mars.mips.hardware.RegisterFile;
import mars.mips.moss.filemanager.Directory;
import mars.mips.moss.filemanager.File;
import mars.mips.moss.filemanager.FileSystem;
import mars.mips.moss.processmanager.ProcessControlBlock;
import mars.mips.moss.processmanager.ProcessController;
import mars.mips.moss.processmanager.ProcessTable;
import mars.tools.MIPSOperatingSystemSimulator;
import mars.util.SystemIO;

public class SyscallMOSS extends AbstractSyscall {
			
	private int numCall; 
	public boolean hasLoad = false;

	public SyscallMOSS() {
		super(18, "SyscalMOSS");
		
	}

	@Override
	public void simulate(ProgramStatement statement) throws ProcessingException {
				
		List<Integer> temp = new ArrayList<Integer>();
		for (int i = 0; i < RegisterFile.getRegisters().length;i++){
			temp.add(RegisterFile.getValue(i));
		}
		// Verifica qual op��o do sistema foi chamada (1 - Fork. 2 - ProcessChange. 3 - ProcessTerminate.
		switch (RegisterFile.getUserRegister("$t7").getValue()) {
		case 1:
			temp.add(RegisterFile.getValue(33));
			temp.add(RegisterFile.getValue(34));
			int PID = ProcessTable.getNextPID();
			// Cria o novo processo
			ProcessControlBlock pcb = new ProcessControlBlock();
			pcb.setLastInstAdress(RegisterFile.getUserRegister("$a1").getValue());			
			pcb.setFirstInstAdress(RegisterFile.getUserRegister("$a0").getValue());
			pcb.setProcPID(PID);
			pcb.setProcState("ready");
			pcb.setPriority(RegisterFile.getUserRegister("$v1").getValue());
			pcb.setMaxPriority(RegisterFile.getUserRegister("$a2").getValue());
			pcb.setMinPriority(RegisterFile.getUserRegister("$a3").getValue());
			pcb.setContexto(temp);
			pcb.setTimesExec(0);
			pcb.setLastExecInstAdress(RegisterFile.getUserRegister("$a0").getValue());
			pcb.setInstCount((pcb.getLastInstAdress()-pcb.getFirstInstAdress())/4);
			// adicionar o pcb a Tabela de processo
			ProcessTable.addNewPCB(pcb);
			// Coloca o valor do PID no registrador $v1 (Retorna o PID do processo criado)
			RegisterFile.getUserRegister("$v1").setValue(PID);
			ProcessController.updateTable(ProcessTable.getProcessList());
			break;
		case 2:		
			ProcessTable.processChange(ProcessController.getSelectedScheduler());
			break;
		case 3:		
			ProcessTable.processTerminate(ProcessController.getSelectedScheduler());			
			break;
		default:
			
			break;
		}
		
	}

	public int getNumCall() {
		return numCall;
	}

	public void setNumCall(int numCall) {
		this.numCall = numCall;
	}

}
