package mars.mips.moss.memorymanager;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Observable;
import java.util.Vector;

import mars.mips.hardware.AccessNotice;
import mars.mips.hardware.MemoryAccessNotice;
import mars.mips.moss.processmanager.ProcessTable;
import mars.tools.AbstractMarsToolAndApplication;

public class MemoryController {
	public static JTextField hitsCounts;
	public static JTextField missCounts;
	public static JTextField SubsTPagina;
	public static JTextField TempoTotal;
	public static JTextField TempoSubst;
	public static JTextField TempoAcessoMem;
	public static JTextField TempoAcessoDisco;

	JTextArea JLog;

	public static JComboBox<Integer> qtdPaginaMemVirtual;
	public static JComboBox<Integer> tamPagina;
	public static JComboBox<Integer> qtdMolduraMemFisica;
	public static JComboBox<String> metodoPaginacao;

	private static JTable table;
	private static JTextArea hitMissMessage;

	public JComponent buildMainDisplayArea() {

		Box memoryBox = Box.createHorizontalBox();
		TitledBorder tc = new TitledBorder("Memory Manager");
		tc.setTitleJustification(TitledBorder.CENTER);
		memoryBox.setBorder(tc);

		JPanel painel = new JPanel();

		BorderLayout layout = new BorderLayout();
		layout.setVgap(10);
		layout.setHgap(10);
		painel.setLayout(layout);

		JScrollPane scrollPane = new JScrollPane();
		painel.add(scrollPane);

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Instruction PID", "Page table index", "Memory frame" }) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			Class[] columnTypes = new Class[] { String.class, String.class, String.class };

			public Class<?> getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			boolean[] columnEditables = new boolean[] { false, true };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.setPreferredScrollableViewportSize(new Dimension(200, 200));
		scrollPane.setViewportView(table);

		hitMissMessage = new JTextArea();
		hitMissMessage.setEditable(false);
		hitMissMessage.setLineWrap(true);
		hitMissMessage.setWrapStyleWord(true);
		hitMissMessage.setFont(new Font("Ariel", Font.PLAIN, 12));

		int valor = 0;
		if (ProcessTable.getExeProc() != null) {
			valor = ProcessTable.getExeProc().getProcPID();
		}
		hitMissMessage.setText(" ");
		hitMissMessage.setCaretPosition(0); // Assure first line is visible and at top of scroll pane.
		painel.add(hitMissMessage);
		// painel.add(buildConfigPanel(), BorderLayout.NORTH);
		painel.add(buildInfoPanel(), BorderLayout.EAST);
		painel.add(buildSelectPainel(), BorderLayout.WEST);
		painel.add(scrollPane);

		memoryBox.add(painel);
		return memoryBox;
	}

	private JPanel buildSelectPainel() {
		Vector<Integer> tamPg = new Vector<Integer>();
		tamPg.add(new Integer(4));
		tamPg.add(new Integer(8));
		tamPg.add(new Integer(16));
		tamPg.add(new Integer(32));

		Vector<Integer> qtdBloc = new Vector<Integer>();
		qtdBloc.add(new Integer(4));
		qtdBloc.add(new Integer(8));
		qtdBloc.add(new Integer(16));
		qtdBloc.add(new Integer(32));

		Vector<Integer> qtdMemFis = new Vector<Integer>();
		qtdMemFis.add(new Integer(4));
		qtdMemFis.add(new Integer(8));
		qtdMemFis.add(new Integer(16));
		qtdMemFis.add(new Integer(32));

		Vector<String> metodos = new Vector<String>();
		metodos.add("NRU");
		metodos.add("FIFO");
		metodos.add("Second Chance");
//		metodos.add("LRU");

		metodoPaginacao = new JComboBox<String>(metodos);
		metodoPaginacao.setSelectedIndex(1);
		qtdPaginaMemVirtual = new JComboBox<Integer>(qtdBloc);
		qtdPaginaMemVirtual.setSelectedIndex(2);
		qtdMolduraMemFisica = new JComboBox<Integer>(qtdMemFis);
		tamPagina = new JComboBox<Integer>(tamPg);

		metodoPaginacao.setAlignmentX(AbstractMarsToolAndApplication.CENTER_ALIGNMENT);
		qtdPaginaMemVirtual.setAlignmentX(AbstractMarsToolAndApplication.CENTER_ALIGNMENT);
		qtdMolduraMemFisica.setAlignmentX(AbstractMarsToolAndApplication.CENTER_ALIGNMENT);
		tamPagina.setAlignmentX(AbstractMarsToolAndApplication.CENTER_ALIGNMENT);

		JPanel panel = new JPanel();
		JPanel outerPanel = new JPanel();
		outerPanel.setLayout(new BorderLayout());

		GridBagLayout gbl = new GridBagLayout();
		panel.setLayout(gbl);

		GridBagConstraints c = new GridBagConstraints();

		c.insets = new Insets(5, 5, 2, 5);
		c.gridx = 1;
		c.gridy = 1;

		panel.add(new JLabel("# Pages"), c);
		c.gridy++;
		panel.add(qtdPaginaMemVirtual, c);

		c.gridy++;
		panel.add(new JLabel("Instructions/Page"), c);
		c.gridy++;
		panel.add(tamPagina, c);

		c.gridy++;
		panel.add(new JLabel("Memory Size"), c);
		c.gridy++;
		panel.add(qtdMolduraMemFisica, c);

		c.gridy++;
		panel.add(new JLabel("Paging method"), c);
		c.gridy++;
		panel.add(metodoPaginacao, c);

		outerPanel.add(panel, BorderLayout.NORTH);
		return outerPanel;
	}

	private JPanel buildInfoPanel() {
		hitsCounts = new JTextField();
		hitsCounts.setColumns(10);
		hitsCounts.setEditable(false);
		hitsCounts.setAlignmentX(AbstractMarsToolAndApplication.CENTER_ALIGNMENT);
		hitsCounts.setText(String.valueOf(0));

		missCounts = new JTextField();
		missCounts.setColumns(10);
		missCounts.setEditable(false);
		missCounts.setAlignmentX(AbstractMarsToolAndApplication.CENTER_ALIGNMENT);
		missCounts.setText(String.valueOf(0));

		// message = new JTextField();
		// message.setColumns(10);
		// message.setEditable(false);
		// message.setAlignmentX(CENTER_ALIGNMENT);

		SubsTPagina = new JTextField();
		SubsTPagina.setColumns(10);
		SubsTPagina.setEditable(false);
		SubsTPagina.setAlignmentX(AbstractMarsToolAndApplication.CENTER_ALIGNMENT);
		SubsTPagina.setText(String.valueOf(0));
		//
		// TempoTotal = new JTextField();
		// TempoTotal.setColumns(10);
		// TempoTotal.setEditable(false);
		// TempoTotal.setAlignmentX(CENTER_ALIGNMENT);
		//
		// TempoAcessoDisco = new JTextField();
		// TempoAcessoDisco.setColumns(10);
		// TempoAcessoDisco.setEditable(false);
		// TempoAcessoDisco.setAlignmentX(CENTER_ALIGNMENT);
		//
		// TempoAcessoMem = new JTextField();
		// TempoAcessoMem.setColumns(10);
		// TempoAcessoMem.setEditable(false);
		// TempoAcessoMem.setAlignmentX(CENTER_ALIGNMENT);
		//
		// TempoSubst = new JTextField();
		// TempoSubst.setColumns(10);
		// TempoSubst.setEditable(false);
		// TempoSubst.setAlignmentX(CENTER_ALIGNMENT);

		JPanel panel = new JPanel();
		JPanel outerPanel = new JPanel();
		outerPanel.setLayout(new BorderLayout());

		GridBagLayout gbl = new GridBagLayout();
		panel.setLayout(gbl);

		GridBagConstraints c = new GridBagConstraints();

		c.insets = new Insets(5, 5, 2, 5);
		c.gridx = 1;
		c.gridy = 1;

		panel.add(new JLabel("Hits"), c);
		c.gridy++;
		panel.add(hitsCounts, c);

		c.gridy++;
		panel.add(new JLabel("Misses"), c);
		c.gridy++;
		panel.add(missCounts, c);

		// c.gridy++;
		// panel.add(new JLabel("PC"), c);
		// c.gridy++;
		// panel.add(message, c);

		c.gridy++;
		panel.add(new JLabel("Pages replacements"), c);
		c.gridy++;
		panel.add(SubsTPagina, c);
		//
		// c.gridy++;
		// panel.add(new JLabel("Tempo de acesso a Memoria"), c);
		// c.gridy++;
		// panel.add(TempoAcessoMem, c);
		//
		// c.gridy++;
		// panel.add(new JLabel("Tempo de acesso ao disco"), c);
		// c.gridy++;
		// panel.add(TempoAcessoDisco, c);
		//
		// c.gridy++;
		// panel.add(new JLabel("Tempo de Substitui�oes"), c);
		// c.gridy++;
		// panel.add(TempoSubst, c);
		//
		// c.gridy++;
		// panel.add(new JLabel("Tempo Total"), c);
		// c.gridy++;
		// panel.add(TempoTotal, c);

		outerPanel.add(panel, BorderLayout.NORTH);
		return outerPanel;
	}

	public static void updateDisplay(String[] dados) {
		((DefaultTableModel) table.getModel()).addRow(dados);
	}

	public static void updateDisplay(int indexRemove) {
		((DefaultTableModel) table.getModel()).removeRow(indexRemove);
	}

	public static void updateDisplay(String dados, int index, int columm) {
		((DefaultTableModel) table.getModel()).setValueAt(dados, index, columm);
	}

	public void reset() {
		
	}

	public void initializePreGUI() {		
		
	}

	public void updateDisplay() {
		
	}

	public void processMIPSUpdate(Observable memory, AccessNotice notice, int a) {		
		MemoryManager.VerificarMemoria(a);
	}

	public void initializePostGUI() {
		// TODO Auto-generated method stub
		
	}	

}