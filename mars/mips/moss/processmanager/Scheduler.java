package mars.mips.moss.processmanager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Scheduler {
		
	public Scheduler(){
		
	}	
	/**
	 * Efetua o roteamento entre o processo atual para um novo processo
	 */
	public static boolean FILA(){		
		if (ProcessTable.getExeProc() == null) {
			ProcessControlBlock temp = ProcessTable.getProcessList().get(0);
			temp.setProcState("running");
			ProcessTable.setExeProc(temp);
			return true;
		}else {
			ProcessControlBlock temp = ProcessTable.getExeProc();
			ProcessTable.getProcessList().remove(temp);
			ProcessTable.getProcessList().add(temp);
			temp = ProcessTable.getProcessList().get(0);
			temp.setProcState("running");
			ProcessTable.setExeProc(temp);
			return true;
		}
	}
	
	public static boolean prioridadeFixa(){		
		if (ProcessTable.getExeProc() == null) {
			Collections.sort(ProcessTable.getProcessList());
			ProcessControlBlock temp = ProcessTable.getProcessList().get(0);
			temp.setProcState("running");
			ProcessTable.setExeProc(temp);
			return true;
		}else {
			ProcessControlBlock temp = ProcessTable.getProcessList().get(0);
			temp.setProcState("running");
			ProcessTable.setExeProc(temp);
			return true;
		}
	}
	
	public static boolean prioridadeDinamica(){	
		Collections.sort(ProcessTable.getProcessList());
		if (ProcessTable.getExeProc() == null) {
			ProcessControlBlock temp = ProcessTable.getProcessList().get(0);
			temp.setProcState("running");
			// incrementa o contador de vezes executado na dinamica
			temp.setQueueState(temp.getQueueState()+1);
			// verifica se o contador é maior que a diferença entre sua prioridade e sua prioridade máxima
						if (temp.getQueueState() > temp.getMaxPriority()-temp.getPriority()) {
							// zera o contador
							temp.setQueueState(0);
							// verifica se pode decrementar a prioridade
							if (temp.getPriority()-1 >= temp.getMinPriority()) {
								// decrementa a prioridade do processo
								temp.setPriority(temp.getPriority()-1);
							}
						}
			ProcessTable.setExeProc(temp);
			// para cada processo que não esteja executando, decrementa o contador de vezes executado na dinamica.
			for (ProcessControlBlock process : ProcessTable.getProcessList()) {
				if (process != temp) {
					process.setQueueState(process.getQueueState()-1);
					// verifica se o contador é menor que a diferença entre sua prioridade e sua prioridade máxima
					if (process.getQueueState() < process.getPriority()-process.getMaxPriority()) {
						// verifica se pode incrementar a prioridade
						if (process.getPriority()+1 > process.getMaxPriority()) {
							// incrementa a prioridade do processo
							process.setPriority(process.getPriority()+1);
						}
					}
				}
			}
			return true;
		}else {
			ProcessControlBlock temp = ProcessTable.getProcessList().get(0);
			temp.setProcState("running");
			// incrementa o contador de vezes executado na dinamica
			ProcessTable.setExeProc(temp);
			temp.setQueueState(temp.getQueueState()+1);
			// verifica se o contador é maior que a diferença entre sua prioridade e sua prioridade máxima
			if (temp.getQueueState() > temp.getMaxPriority()-temp.getPriority()) {
				// zera o contador
				temp.setQueueState(0);
				// verifica se pode decrementar a prioridade
				if (temp.getPriority()-1 >= temp.getMinPriority()) {
					// decrementa a prioridade do processo
					temp.setPriority(temp.getPriority()-1);
				}
			}
			ProcessTable.setExeProc(temp);
			// para cada processo que não esteja executando, decrementa o contador de vezes executado na dinamica.
			for (ProcessControlBlock process : ProcessTable.getProcessList()) {
				if (process != temp) {
					process.setQueueState(process.getQueueState()-1);
					// verifica se o contador é menor que a diferença entre sua prioridade e sua prioridade minima
					if (process.getQueueState() < process.getMinPriority()-process.getPriority()) {
						// zera o contador
						process.setQueueState(0);
						// verifica se pode incrementar a prioridade
						if (process.getPriority()+1 >= process.getMaxPriority()) {
							// incrementa a prioridade do processo
							process.setPriority(process.getPriority()+1);
						}
					}
				}
			}return true;
		}
		
	}
	public static boolean loteria(){		
		Random rd = new Random();
		int lotery = rd.nextInt(ProcessTable.getProcessList().size());
		if (ProcessTable.getExeProc() == null) {
			ProcessControlBlock temp = ProcessTable.getProcessList().get(lotery);
			temp.setProcState("running");
			ProcessTable.setExeProc(temp);
			return true;
		}else {
			ProcessControlBlock temp = ProcessTable.getProcessList().get(lotery);
			temp.setProcState("running");
			ProcessTable.setExeProc(temp);
			return true;
		}
		
	}

}
