package mars.mips.moss.processmanager;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import mars.mips.hardware.RegisterFile;
import mars.util.SystemIO;

public class ProcessTable {	
	
	/**
	 * Processo que esta executando no momento
	 */
	private static ProcessControlBlock exeProc;	
	/**
	 * Lista de processos existente no escalonador
	 */
	private static ArrayList<ProcessControlBlock> processList = new ArrayList<ProcessControlBlock>();
	/**
	 * PID do ultimo processo criado
	 */
	private static int previousPID = 0;
	
	public ProcessTable(ProcessControlBlock exeProc) {		
		ProcessTable.exeProc = exeProc;		
	}	
	
	/**
	 * 
	 * @param escalonador
	 */
	public static void processChange(int escalonador){
		int oldPID = -1;		
		if (exeProc != null){	
			oldPID = exeProc.getProcPID();
			//Coloca o processo no estado "pronto"
			exeProc.setProcState("ready");			
			//Incrementa o contador de vezes executada
			exeProc.setTimesExec(exeProc.getTimesExec()+1);
			//Atualizar a instrução atual que seria executada
			exeProc.setLastExecInstAdress(RegisterFile.getProgramCounter());
			//Atualiza o contexto
			exeProc.getContexto().clear();
			for (int i = 0; i < RegisterFile.getRegisters().length;i++){
				exeProc.getContexto().add(RegisterFile.getValue(i));
			}
			exeProc.getContexto().add(RegisterFile.getValue(33));
			exeProc.getContexto().add(RegisterFile.getValue(34));			
		}
		
		switch (escalonador){
		case 1:			
			if (Scheduler.FILA()){
				RegisterFile.setProgramCounter(exeProc.getLastExecInstAdress());
				for (int i = 0; i < exeProc.getContexto().size();i++){
					RegisterFile.updateRegister(i, exeProc.getContexto().get(i));
				}
			}			
			break;
		case 2:
			if (Scheduler.prioridadeFixa()){
				RegisterFile.setProgramCounter(exeProc.getLastExecInstAdress());
				for (int i = 0; i < exeProc.getContexto().size();i++){
					RegisterFile.updateRegister(i, exeProc.getContexto().get(i));
				}
			}	
			break;
		case 3:
			if (Scheduler.prioridadeDinamica()){
				RegisterFile.setProgramCounter(exeProc.getLastExecInstAdress());
				for (int i = 0; i < exeProc.getContexto().size();i++){
					RegisterFile.updateRegister(i, exeProc.getContexto().get(i));
				}
			}	
			break;
		case 4:
			if (Scheduler.loteria()){
				RegisterFile.setProgramCounter(exeProc.getLastExecInstAdress());
				for (int i = 0; i < exeProc.getContexto().size();i++){
					RegisterFile.updateRegister(i, exeProc.getContexto().get(i));
				}
			}	
			break;
		default:
			break;
		}
		// Se trocar o precesso por ele mesmo o registrador PC nao sera alterado, logo n sera chamado a instrucao q ativa o incremento do pc
		if (oldPID == exeProc.getProcPID())
			ProcessController.enableExec();
		
	}
	/**
	 * 
	 * caso queira adicionar outros m�todos de escalonamento � s� inserir no switch case e alterar o parametro do processChange
	 * 
	 * @param escalonador
	 */
	public static void processTerminate(int escalonador){
		processList.remove(exeProc);
		exeProc = null;
		processChange(escalonador);
	}
	
	/**
	 * adiciona novo PCB a lista
	 * @param pcb
	 */
	public static void addNewPCB(ProcessControlBlock pcb){
		processList.add(pcb);
	}
	
	public static ProcessControlBlock getExeProc() {
		return exeProc;
	}
	public static void setExeProc(ProcessControlBlock exeProc) {
		ProcessTable.exeProc = exeProc;
	}

	public static void setProcessList(ArrayList<ProcessControlBlock> processList) {
		ProcessTable.processList = processList;
	}
	
	public static ArrayList<ProcessControlBlock> getProcessList(){
		return processList;
	}
	
	public static int getFirstAdress(){
		int menor = Integer.MAX_VALUE;
		for (ProcessControlBlock pcb : processList) {
			if (pcb.getLastExecInstAdress() < menor)
				menor = pcb.getLastExecInstAdress();
		}
				
		return menor;
		
	}

	public static int getNextPID() {		
		return ++previousPID;
	}
	
	public static void setPreviousPID(int pid) {
		previousPID = pid;
	}

}
