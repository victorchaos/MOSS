package mars.mips.moss.processmanager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Observable;
import java.util.PriorityQueue;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import mars.mips.hardware.AccessNotice;
import mars.mips.hardware.MemoryAccessNotice;
import mars.util.SystemIO;

public class ProcessController {
	/**
	 * --------------- Process Manager variables -----------------------------
	 **/
	/**
	 * Boolean that determinates if the processChange will be called
	 */
	private static boolean canExec = true;

	private static JToggleButton timerOn;

	protected int lastAddress = -1; // comparativo de endereço
	/**
	 * Number of instructions executed until now.
	 */
	protected int counter = 0;
	private JTextField counterField;

	/**
	 * Escalonador utulizado
	 */
	protected static int selectedScheduler = 1;	
	/**
	 * Number of interruptions until now.
	 */
	protected int countInter = 0; // contador de interrupções
	private JTextField counterInterField;
	/**
	 * Number of instructions until interruption.
	 */
	protected int countInst = 3;
	private JProgressBar progressbarInst;

	/**
	 * Configuration tools
	 */
	private JSpinner timerConfig;
	
	private static JTable table;

	/**
	 * ---------------------------------------------Select Options components------------------------------------------
	 **/
	public static JComboBox<String> schedulerSelector;
	
	public JComponent buildMainDisplayArea() {
		Box proc = Box.createHorizontalBox();
		TitledBorder tc = new TitledBorder("Process Manager");
		tc.setTitleJustification(TitledBorder.CENTER);
		proc.setBorder(tc);	
		
		//Creating the north panel (Config + info)
		JPanel northPanel = new JPanel(new GridBagLayout());
		proc.add(northPanel);
		
		counterField = new JTextField("0", 10);
		counterField.setEditable(false);

		counterInterField = new JTextField("0", 10);
		counterInterField.setEditable(false);

		progressbarInst = new JProgressBar(JProgressBar.HORIZONTAL);
		progressbarInst.setStringPainted(true);

		timerOn = new JToggleButton("OFF");
		timerOn.setBackground(new Color(230, 66, 25));
		timerOn.setToolTipText("Enable/Disable interruption");

		timerConfig = new JSpinner();
		timerConfig.setModel(new SpinnerNumberModel(10, 2, 100, 1));
		timerConfig.setEditor(new JSpinner.NumberEditor(timerConfig));
		// isso meu amigo, eu chamo de gambiarra.
		((JSpinner.DefaultEditor)timerConfig.getEditor()).getTextField().setEditable(false);
		timerConfig.setToolTipText("Sets the time for the interruption");		

		// Add them to the panel
		JSeparator s = new JSeparator();

		// Fields
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		northPanel.add(s, c);
		c.gridheight = c.gridwidth = 1;
		c.gridx = 3;
		c.gridy = 1;
		c.insets = new Insets(0, 0, 5, 5);

		northPanel.add(new JLabel("Instructions so far: "), c);
		c.gridx++;
		northPanel.add(counterField, c);
		c.gridx++;

		northPanel.add(new JLabel("Interruptions so far: "), c);
		c.gridx++;
		northPanel.add(counterInterField, c);
		c.gridx++;

		northPanel.add(new JLabel("Timer: "), c);
		c.gridx++;
		northPanel.add(timerConfig, c);
		c.gridy++;

		c.gridx = 3;
		northPanel.add(new JLabel("Time so far: "), c);
		c.gridx++;
		northPanel.add(progressbarInst, c);
		c.gridx++;
		timerOn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getActionCommand().equals("OFF")) {
					timerOn.setBackground(new Color(61, 143, 61));					
					timerOn.setText("ON");
				} else {
					timerOn.setBackground(new Color(230, 66, 25));
					timerOn.setText("OFF");
				}

			}
		});
		northPanel.add(new JLabel("Preemptive schedule:"), c);
		c.gridx++;
		northPanel.add(timerOn, c);
		c.gridx++;
		northPanel.add(new JLabel("Scheduler: "), c);
		c.gridx++;

		Vector<String> metodos = new Vector<String>();
		metodos.add("Queue");
		metodos.add("Fixed priority");
		metodos.add("Dinamic priority");
		metodos.add("Lottery");

		schedulerSelector = new JComboBox<String>(metodos);
		schedulerSelector.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedScheduler = schedulerSelector.getSelectedIndex() + 1;
			}
		});
		schedulerSelector.setSelectedIndex(0);
		selectedScheduler = 1;	

		northPanel.add(schedulerSelector, c);		

		c.anchor = GridBagConstraints.CENTER;
		c.gridy++;
		c.gridx = 3;
		c.gridwidth = 6;
		
		JScrollPane scrollPane = new JScrollPane();		
		northPanel.add(scrollPane, c);	
		
		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Process ID", "Status", "CPU (%)","# Instructions", "# Executions", "Priority" }) {
			Class[] columnTypes = new Class[] { Integer.class, String.class, String.class,Integer.class, Integer.class, Integer.class, Integer.class };

			public Class<?> getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			boolean[] columnEditables = new boolean[] { false, true };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		
		for (int i = 0; i < table.getColumnModel().getColumnCount(); i++) {
			table.getColumnModel().getColumn(i).setResizable(false);
		}		
		table.setPreferredScrollableViewportSize(new Dimension(690, 150));
		scrollPane.setViewportView(table);
		
		return proc;
	}

	public void initializePreGUI() {
		counter = 0;
		countInst = 0;
		countInter = 0;
		lastAddress = -1;		
		
	}

	public void processMIPSUpdate(Observable memory, AccessNotice notice, int a) {
		if (a == lastAddress) return;
		enableExec();
		if (ProcessTable.getExeProc() != null) {
			++counter;
			ProcessTable.getExeProc().setExecInstCount(ProcessTable.getExeProc().getExecInstCount()+1);
			for (ProcessControlBlock process : ProcessTable.getProcessList()) {
				process.setCPUUsage(((double)process.getExecInstCount()/counter)*100);
			}
			ProcessController.updateTable(ProcessTable.getProcessList());
			lastAddress = a;			
			if(timerOn.isSelected()) {
				++countInst;
				if (countInst > (int)timerConfig.getValue()) {
					disableExec();										
					++countInter; // incrementa interrupções
					countInst = 0; // zera o progressBar				
					ProcessTable.processChange(selectedScheduler);
				}
			}
		}
	}

	public void updateDisplay() {
		counterField.setText(String.valueOf(counter));
		counterInterField.setText(String.valueOf(countInter));
		progressbarInst.setValue(countInst);
		progressbarInst.setMaximum((int)timerConfig.getValue());
		
	}
	
	public static void updateTable(ArrayList<ProcessControlBlock> processList) {
		((DefaultTableModel) table.getModel()).setRowCount(0);		
		for (ProcessControlBlock process : processList) {			
			((DefaultTableModel) table.getModel()).addRow(pcbToVector(process));
		}	
	}
	
	public static Vector<Object> pcbToVector(ProcessControlBlock process){
		Vector<Object> vec = new Vector<Object>();
		vec.add(process.getProcPID());
		vec.add(process.getProcState());
		DecimalFormat df = new DecimalFormat("0.##");
		vec.add(df.format(process.getCPUUsage()) + "%");
		vec.add(process.getInstCount());
		vec.add(process.getTimesExec());
		vec.add(process.getPriority());	
		
		return vec;
	}

	public void reset() {
		ProcessTable.setPreviousPID(0);
		initializePreGUI();
		updateDisplay();
		ProcessTable.getProcessList().clear();
		ProcessTable.setExeProc(null);
		((DefaultTableModel) table.getModel()).setRowCount(0);
	}

	public static int getSelectedScheduler() {
		return selectedScheduler;
	}

	public static boolean canExec() {
		return canExec;
	}

	public static void enableExec() {
		canExec = true;		
	}
	
	public static void disableExec() {
		canExec = false;	
	}

	public void initializePostGUI() {
		// TODO Auto-generated method stub
		
	}

}
