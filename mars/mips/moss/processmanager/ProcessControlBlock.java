package mars.mips.moss.processmanager;

import java.util.List;

import mars.mips.hardware.Register;

public class ProcessControlBlock implements Comparable<ProcessControlBlock>{	
	
	/**
	 * Lista de registradores
	 */
	private List<Integer> contexto;
	/**
	 * Endere�o da instru��o a ser executada do processo
	 */
	private int lastExecInstAdress;
	/**
	 * Endereco da primeira instrucao do processo
	 */
	private int firstInstAdress;
	/**
	 * Endereco da ultima instrucao do processo
	 */
	private int lastInstAdress;
	/**
	 * Processo ID(endereco do label)
	 */
	private int procPID;
	/**
	 * Estado do processo (Executando, Pronto, *Bloqueado*)
	 */
	private String procState;	
	/**
	 * 	Prioridade fixa do processo
	 */
	private int priority;
	/**
	 * 	Prioridade maxima do processo
	 */
	private int maxPriority;
	/**
	 * 	Prioridade minima do processo
	 */
	private int minPriority;
	/**
	 * 	Quantidade de vezes que foi executado sem ser escalonado
	 */
	private int timesExec = 0;
	/**
	 * Quantidade de instrucoes executadas pelo processo
	 */
	private int instCount = 0;
	/**
	 * Contador de instrucoes executadas pelo processo
	 */
	private int execInstCount = 0;
	/**
	 * Uso de CPU (%) = instrucoes executadas pelo processo/total de instrucoes executadas
	 */
	private double CPUUsage = 0;
	/**
	 * Uso para o escalonamento dinamico
	 */
	private int queueState;
	
	/* TRABALHO 2.0
	 * 
	 * Adicione atributos na sua classe PCB (Process Control Block) para manter
		informa��es a respeito do gerenciamento de mem�ria do processo tais como:
		o Registrador de limite superior da mem�ria do processo
		o Registrador de limite inferior da mem�ria do processo
		o Entre outros que for necess�rio para gerenciamento de mem�ria
	 * 
	 * 
	 * */
	
	private String regLimSup;
	private String regLimInf;
	
	public ProcessControlBlock(int endInicio, int pidProc,
			String estadoProc, List<Integer> contexto) {
		super();
		this.lastExecInstAdress = endInicio;
		this.procPID = pidProc;
		this.procState = estadoProc;
		this.setContexto(contexto);		
	}
	
	public ProcessControlBlock(int endInicio, int pidProc,
			String estadoProc, int prioridade, List<Integer> contexto) {
		super();
		this.lastExecInstAdress = endInicio;
		this.procPID = pidProc;
		this.procState = estadoProc;
		this.priority = prioridade;
		this.setContexto(contexto);
	}
	
	public ProcessControlBlock(int endInicio, int endAdress,int pidProc,
			String estadoProc, int prioridade, int prioridadeMax, int prioridadeMin, List<Integer> contexto) {
		super();
		this.lastExecInstAdress = endInicio;
		this.firstInstAdress = endInicio;
		this.procPID = pidProc;
		this.procState = estadoProc;
		this.priority = prioridade;
		this.maxPriority = prioridadeMax;
		this.minPriority = prioridadeMin;
		this.setContexto(contexto);
		this.timesExec = 0;
		this.lastInstAdress = endAdress;
		this.instCount = (lastInstAdress-firstInstAdress)/4;
	}
	
	public ProcessControlBlock() {
		super();
	}

	@Override
	public String toString(){
		return lastExecInstAdress +" "+ procPID +" "+ procState;
	}
	public List<Integer> getContexto() {
		return contexto;
	}
	public void setContexto(List<Integer> contexto) {
		this.contexto = contexto;
	}
	public int getLastExecInstAdress() {
		return lastExecInstAdress;
	}
	public void setLastExecInstAdress(int endInicio) {
		this.lastExecInstAdress = endInicio;
	}
	public int getProcPID() {
		return procPID;
	}
	public void setProcPID(int pidProc) {
		this.procPID = pidProc;
	}
	public String getProcState() {
		return procState;
	}
	public void setProcState(String estadoProc) {
		this.procState = estadoProc;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int prioridade) {
		this.priority = prioridade;
	}

	public int getMaxPriority() {
		return maxPriority;
	}

	public void setMaxPriority(int prioridadeMax) {
		this.maxPriority = prioridadeMax;
	}

	public int getMinPriority() {
		return minPriority;
	}

	public void setMinPriority(int prioridadeMin) {
		this.minPriority = prioridadeMin;
	}

	public int getTimesExec() {
		return timesExec;
	}

	public void setTimesExec(int timesExec) {
		this.timesExec = timesExec;
	}

	public String getRegLimSup() {
		return regLimSup;
	}

	public void setRegLimSup(String regLimSup) {
		this.regLimSup = regLimSup;
	}

	public String getRegLimInf() {
		return regLimInf;
	}

	public void setRegLimInf(String regLimInf) {
		this.regLimInf = regLimInf;
	}

	public int getFirstInstAdress() {
		return firstInstAdress;
	}

	public void setFirstInstAdress(int startAdress) {
		this.firstInstAdress = startAdress;
	}

	public int getLastInstAdress() {
		return lastInstAdress;
	}

	public void setLastInstAdress(int endAdress) {
		this.lastInstAdress = endAdress;
	}

	public int getInstCount() {
		return instCount;
	}

	public void setInstCount(int instCount) {
		this.instCount = instCount;
	}

	public int getQueueState() {
		return queueState;
	}

	public void setQueueState(int queueState) {
		this.queueState = queueState;
	}

	public int getExecInstCount() {
		return execInstCount;
	}

	public void setExecInstCount(int execInstCount) {
		this.execInstCount = execInstCount;
	}

	public double getCPUUsage() {
		return CPUUsage;
	}

	public void setCPUUsage(double cPUUsage) {
		CPUUsage = cPUUsage;
	}

	@Override
	public int compareTo(ProcessControlBlock arg0) {
		if (this.getPriority() > arg0.getPriority()) {
			return -1;
		}else if (this.getPriority() < arg0.getPriority()) {
			return 1;
		}else {
			return 0;
		}
	}	
	
}
