package mars.mips.moss.filemanager;

public class SuperBlock {

	private static int wordSize = (int) FileManagerController.getIntComboBoxSelection(FileManagerController.wordSize);; // tamanho dos blocos de alocação
	private static int diskSize = (int) FileManagerController.getIntComboBoxSelection(FileManagerController.diskSize); // quantidade de blocos do disco
	private static int [] blocks = new int[diskSize]; // bloco de disco
	
	
	public SuperBlock() {
		
	}	
	
	public static int putNextWord(File file) {
		for (int i = 0; i < blocks.length; i++) {
			if (blocks[i] == 0) {
				blocks[i] = file.getDescritor();
				return i;
			}
		}
		return -1;
	}

	public int [] getBlocks() {
		return blocks;
	}

	public void setBlocks(int [] blocks) {
		SuperBlock.blocks = blocks;
	}


	public static int getWordSize() {
		return wordSize;
	}


	public static void setWordSize(int sizeBlock) {
		SuperBlock.wordSize = sizeBlock;
	}


	public static int getDiskSize() {
		return diskSize;
	}


	public static void setDiskSize(int blocksAmount) {
		SuperBlock.diskSize = blocksAmount;
	}

	public static void allocWordAt(int fd, int index) {
		blocks[index] = fd;	
	}
	
	
}
