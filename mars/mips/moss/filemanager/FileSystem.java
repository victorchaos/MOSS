package mars.mips.moss.filemanager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mars.mips.moss.processmanager.ProcessTable;

public class FileSystem {
	
	private static SuperBlock suBlock;
	static RuntimeTypeAdapterFactory<File> fileAdapterFactory = RuntimeTypeAdapterFactory.of(File.class, "type")
		    .registerSubtype(File.class, "File")
		    .registerSubtype(Directory.class, "Dir");
	private static Gson gson = new GsonBuilder().registerTypeAdapterFactory(fileAdapterFactory).excludeFieldsWithoutExposeAnnotation().create();
	private static ArrayList<File> openedFileList = new ArrayList<File>();
	private static File nodeFile;
	public static String homeDir = "filesystem/";

	public FileSystem() {
		
	}

	public static SuperBlock getSuBlock() {
		return suBlock;
	}

	public static void setSuBlock(SuperBlock suBlock) {
		FileSystem.suBlock = suBlock;
	}

	public static String getHomeDir() {
		return homeDir;
	}

	public static void setHomeDir(String homeDir) {
		FileSystem.homeDir = homeDir;
	}
	
	public static ArrayList<File> getOpenedFileList() {
		return openedFileList;
	}

	public static void setOpenedFileList(ArrayList<File> openedFileList) {
		FileSystem.openedFileList = openedFileList;
	}

	public static File getNodeFile() {
		return nodeFile;
	}

	public static void setNodeFile(File nodeFile) {
		FileSystem.nodeFile = nodeFile;
	}

	public static File openFile(String filename, int mode) {
		// Tentando abrir arquivo: " + filename 
		filename = ProcessTable.getExeProc().getProcPID()+"/"+filename;		
		String [] files = filename.split("/");
		Directory dir = (Directory) getNodeFile();
		for (String nome : files) {
			if (!nome.contains(".")) { 
				// eh um diretorio
				Directory aux = (Directory) dir.hasFile(nome);	
				if (aux != null) {
					// diretorio existe
					// "Diretorio " + path + "/ existe, abrindo..
					dir = aux;
				}else {
					// diretorio nao existe
					// Diretorio " + path + "/ nao existe, criando diretorio..
					if (mode == 1 || mode == 9) {
						// se modo de escrita: criar novo dir												
						aux = new Directory(nome, dir);	
						aux.setDateBirth(new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()));
						dir.add(aux);
						// aux.getName() + "/ criado
						aux.setTreeNode(FileManagerController.treePanel.addObject(dir.getTreeNode(), aux.getName()));					
						dir = aux;
					}else {
						// se mode de leitura o dir deve exixtir
						return null;
					}
				}
			}else {
				// eh arquivo
				File aux = dir.hasFile(nome);
				if (aux != null) {
					// Arquivo " + path + " existe, abrindo...\n  Arquivo " + path aberto
					//arquivo existe, retorna o arqvuivo
					aux.setMode(mode);
					aux.setPath(homeDir+filename.replace("/", "-"));
					return aux;
				}else{
					// arquivo nao existe
					// Arquivo " +path + " nao existe, criando...\n");
					aux = new File();
					if (mode == 1 || mode == 9) {
						// se mode de escreita, cria o arquivo e seta o path
						aux.setMode(mode);
						aux.setName(nome);
						aux.setIdOwner(ProcessTable.getExeProc().getProcPID());
						aux.setPath(homeDir+filename.replace("/", "-"));
						aux.setDateBirth(new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss").format(Calendar.getInstance().getTime()));
						dir.add(aux);
						dir.setDateChange(new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss").format(Calendar.getInstance().getTime()));
						// aux.getName() + " criado com caminho = aux.getPath().replaceAll("-", "/") + "\n");
						FileManagerController.treePanel.addObject(dir.getTreeNode(), aux.getName());
						return aux;
					}else {
						// se mode de leitura o arquivo deve exixtir
						return null;
					}

				}
			}
		}
		return null;
	}
	
	public static void saveFiles() {
		java.io.File oldFile = new java.io.File("Files.txt");
		oldFile.delete();
		java.io.File newFile = new java.io.File("Files.txt");
		FileWriter arq;
		try {
			arq = new FileWriter(newFile);
			PrintWriter save = new PrintWriter(arq);
			String json = gson.toJson(getNodeFile());
			save.println(json);
			arq.close();
		}catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public static void loadFiles() {		
		try {
			BufferedReader br = new BufferedReader(new FileReader("Files.txt"));
			File file = gson.fromJson(br, Directory.class);
		    br.close();	
		    if (file == null) 
		    	file = new Directory("fileSystem");
		    file.setTreeNode(FileManagerController.treePanel.addObject(file.getName()));
			setNodeFile(file);
			((Directory)file).loadJsTree(getNodeFile());
		} catch (RuntimeException | IOException e){
			e.printStackTrace();
		}
	}	

	public static boolean readFile(int fd) {	
		// Verificando arquivo para leitura com FileDescriptor: " + fd + "\n");
		for (File file : getOpenedFileList()) {
			if (file.getDescritor() == fd) {
				// Arquivo existe no diretorio :" + file.getPath() + "\n");
				return true;
			}				
		}
		/// "Arquivo nao esta aberto");
		return false;
	}

	public static boolean writeFile(int fd, int lenght) {
		// Verificando arquivo para escreita com FileDescriptor: " + fd + "\n");
		File toEdit = ((Directory) nodeFile).getFile(fd);
		// Arquivo existe no diretorio :" + file.getPath() + "\n")
		if (Spacer.alloc(lenght, toEdit)) {
			FileManagerController.updateTable(getOpenedFileList());
			return true;				
		}
		// Arquivo nao esta aberto");
		return false;
	}

	public static void closeFile(int fd) {
		// Verificando arquivo para leitura com FileDescriptor: " + fd + "\n");
		File fil = null;
		for (File file : getOpenedFileList()) {
			if (file.getDescritor() == fd) {
				// fechando arquivo
				fil = file;
			}				
		}
		if (fil != null) {
			getOpenedFileList().remove(fil);
			FileManagerController.updateTable(getOpenedFileList());
		}
	}
	
	
}
