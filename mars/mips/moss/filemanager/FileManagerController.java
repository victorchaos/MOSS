package mars.mips.moss.filemanager;

import mars.mips.hardware.AccessNotice;
import mars.mips.moss.processmanager.ProcessControlBlock;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import java.util.*;

public class FileManagerController {

	private JPanel canvas;
	private JPanel results;

	// Some GUI settings
	private EmptyBorder emptyBorder = new EmptyBorder(4, 4, 4, 4);
	private Font countFonts = new Font("Times", Font.BOLD, 12);
	private Color backgroundColor = Color.WHITE;

	public static JComboBox diskSize;
	public static JComboBox wordSize;
	public static JProgressBar progressbar;
	public static int blockUse = 0; // para definir progressbar

	public static DynamicTree treePanel;
	public static DefaultTreeModel treeModel;
	public static DefaultMutableTreeNode rootNode;
	public static java.util.List<DefaultMutableTreeNode> dir;
	public static int wordsPerUnit;	
	
	public static JTable table;
	
	private final String[] bytesPerWord = { "1", "2", "4", "8"};
	private final int defaultWordsPerUnitIndex = 1;
	private final String[] blockSizing = { "128", "512", "1024", "2048", "8192" };
	private final int defaultBlockSizing = 2;
	
	public static boolean isOpen = false;


	// The next four are initialized dynamically in initializeDisplayBaseChoices()
	private String[] displayBaseAddressChoices;
	private int[] displayBaseAddresses;
	private int defaultBaseAddressIndex;
	private int baseAddress;

	private static String heading = "File Manager";
	private static String version = " Version 0.1";
	public static int lastAdress = -1; // comparativo de endereço
	
	public JComponent buildMainDisplayArea() {
		Box file = Box.createHorizontalBox();
		TitledBorder tc = new TitledBorder("File manager");
		tc.setTitleJustification(TitledBorder.CENTER);
		file.setBorder(tc);

		JPanel panel = new JPanel();

		BorderLayout layout = new BorderLayout();
		layout.setVgap(10);
		layout.setHgap(10);
		panel.setLayout(layout);
		panel.add(buildConfigPanel(), BorderLayout.NORTH);		
		panel.add(buildInfoPanel(), BorderLayout.WEST);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(buildTablePanel());
		panel.add(scrollPane, BorderLayout.CENTER);

		file.add(panel);
		return file;
	}

	private JPanel buildInfoPanel() {
		return treePanel = new DynamicTree() {
			public Dimension getPreferredSize() {
				return new Dimension(200, 190);
			}
		};
	}

	private JPanel buildConfigPanel() {
		isOpen = true;
		JPanel panel = new JPanel();
		wordSize = new JComboBox(bytesPerWord);
		wordSize.setEditable(false);
		wordSize.setEditable(false);
		wordSize.setBackground(backgroundColor);
		wordSize.setSelectedIndex(defaultWordsPerUnitIndex);
		wordSize.setToolTipText("Number bytes containing in a word)");
		wordSize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wordsPerUnit = getIntComboBoxSelection(wordSize);
				reset();
			}
		});
		diskSize = new JComboBox(blockSizing);
		diskSize.setEditable(false);
		diskSize.setBackground(backgroundColor);
		diskSize.setSelectedIndex(defaultBlockSizing);
		diskSize.setToolTipText("Number of words representing memory");
		
		progressbar = new JProgressBar(JProgressBar.HORIZONTAL);
		progressbar.setStringPainted(true);

		panel.add(new JLabel("# of DISK entries"));
		panel.add(diskSize);
		panel.add(new JLabel("Bytes per Word"));
		panel.add(wordSize);

		panel.add(new JLabel("Used so far:"));
		panel.add(progressbar);

		return panel;
	}

	private JTable buildTablePanel() {
		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "PID Owner", "File Path", "Allocated Words", "Disk Usage", "FD", "Mode" }) {
			Class[] columnTypes = new Class[] { String.class, String.class, String.class, String.class, String.class, String.class };

			public Class<?> getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}		

			boolean[] columnEditables = new boolean[] { false, true };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.getColumnModel().getColumn(0).setPreferredWidth(40);		
		table.getColumnModel().getColumn(3).setPreferredWidth(40);
		table.getColumnModel().getColumn(4).setPreferredWidth(15);
		table.getColumnModel().getColumn(5).setPreferredWidth(23);
		
		for (int i = 0; i < table.getColumnModel().getColumnCount(); i++) {
			table.getColumnModel().getColumn(i).setResizable(false);
		}
		table.setPreferredScrollableViewportSize(new Dimension(300, 200));
		
		return table;
	}

	public void initializePreGUI() {
	}

	public void reset() {
		rootNode.removeAllChildren();
		treeModel.reload();
		FileSystem.loadFiles();
	}

	public void updateDisplay() {
		// canvas.repaint();
		progressbar.setMinimum(0);
		progressbar.setMaximum(SuperBlock.getDiskSize());
		progressbar.setValue(blockUse);
		DecimalFormat df = new DecimalFormat("0.##");
		double value = ((float)blockUse/SuperBlock.getDiskSize())*100;
		progressbar.setString(String.valueOf(df.format(value) + "%"));
	}

	public static int getIntComboBoxSelection(JComboBox comboBox) {
		try {
			return Integer.parseInt((String) comboBox.getSelectedItem());
		} catch (NumberFormatException nfe) {
			// Can occur only if initialization list contains badly formatted numbers. This
			// is a developer's error, not a user error, and better be caught before
			// release.
			return 1;
		}
	}	

	public class DynamicTree extends JPanel {

		protected JTree tree;
		private Toolkit toolkit = Toolkit.getDefaultToolkit();

		public DynamicTree() {
			super(new GridLayout(1, 0));

			rootNode = new DefaultMutableTreeNode("MOSS");
			treeModel = new DefaultTreeModel(rootNode);
			treeModel.addTreeModelListener(new MyTreeModelListener());
			tree = new JTree(treeModel);
			tree.setEditable(false);
			tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
			tree.setShowsRootHandles(true);

			JScrollPane scrollPane = new JScrollPane(tree);
			add(scrollPane);
		}

		/** Remove all nodes except the root node. */
		public void clear() {
			rootNode.removeAllChildren();
			treeModel.reload();
		}

		/** Remove the currently selected node. */
		public void removeCurrentNode() {
			TreePath currentSelection = tree.getSelectionPath();
			if (currentSelection != null) {
				DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode) (currentSelection.getLastPathComponent());
				MutableTreeNode parent = (MutableTreeNode) (currentNode.getParent());
				if (parent != null) {
					treeModel.removeNodeFromParent(currentNode);
					return;
				}
			}

			// Either there was no selection, or the root was selected.
			toolkit.beep();
		}

		/** Add child to the currently selected node. */
		public DefaultMutableTreeNode addObject(Object child) {
			DefaultMutableTreeNode parentNode = null;
			TreePath parentPath = tree.getSelectionPath();

			if (parentPath == null) {
				parentNode = rootNode;
			} else {
				parentNode = (DefaultMutableTreeNode) (parentPath.getLastPathComponent());
			}

			return addObject(parentNode, child, true);
		}

		public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent, Object child) {
			return addObject(parent, child, false);
		}

		public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent, Object child, boolean shouldBeVisible) {
			DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);

			if (parent == null) {
				parent = rootNode;
			}

			// It is key to invoke this on the TreeModel, and NOT DefaultMutableTreeNode
			treeModel.insertNodeInto(childNode, parent, parent.getChildCount());

			// Make sure the user can see the lovely new node.
			if (shouldBeVisible) {
				tree.scrollPathToVisible(new TreePath(childNode.getPath()));
			}
			return childNode;
		}

		class MyTreeModelListener implements TreeModelListener {
			public void treeNodesChanged(TreeModelEvent e) {
				DefaultMutableTreeNode node;
				node = (DefaultMutableTreeNode) (e.getTreePath().getLastPathComponent());

				/*
				 * If the event lists children, then the changed node is the child of the node
				 * we've already gotten. Otherwise, the changed node and the specified node are
				 * the same.
				 */

				int index = e.getChildIndices()[0];
				node = (DefaultMutableTreeNode) (node.getChildAt(index));

				System.out.println("The user has finished editing the node.");
				System.out.println("New value: " + node.getUserObject());
			}

			public void treeNodesInserted(TreeModelEvent e) {
			}

			public void treeNodesRemoved(TreeModelEvent e) {
			}

			public void treeStructureChanged(TreeModelEvent e) {
			}
		}
	}
	
	public static void updateTable(ArrayList<File> fileList) {
		((DefaultTableModel) table.getModel()).setRowCount(0);		
		for (File file : fileList) {			
			((DefaultTableModel) table.getModel()).addRow(fileToVector(file));
		}	
	}
	
	public static Vector<Object> fileToVector(File file){
		Vector<Object> vec = new Vector<Object>();
		vec.add(file.getIdOwner());
		vec.add(file.getPath().substring(13).replaceAll("-", "/"));
		vec.add(file.getAloccatedWords());
		DecimalFormat df = new DecimalFormat("0.##");
		vec.add(df.format(((double)file.getAloccatedWords()*100)/SuperBlock.getDiskSize()) + "%");
		vec.add(file.getDescritor());
		vec.add(file.getMode());
		
		return vec;
	}

	public void processMIPSUpdate(Observable memory, AccessNotice notice, int a) {
		
	}

	public void initializePostGUI() {	
		FileSystem.loadFiles();	
		updateDisplay();
	}

}
