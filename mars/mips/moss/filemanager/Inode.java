package mars.mips.moss.filemanager;

import java.text.DateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.google.gson.annotations.Expose;

import mars.mips.moss.processmanager.ProcessTable;

public class Inode {	
	private List<FileProtection> rules;	
	private Boolean hide; // flag para ocultar o arquivo
	@Expose
	private int [] blocks = {-1, -1, -1, -1, -1, -1, -1, -1};
	@Expose
	private Inode nextBlock = null;

	/* Os 7 primeiros endereços correspondem aos endereços do disco
	indicados pelo gerenciador de espaço livre
	 O último endereço corresponde a um ponteiro para uma nova estrutura
	com 8 novos endereços, similar a estrutura do i-node, exceto que não
	há atributos */
	
	public Inode(String name, int idOwner, List<FileProtection> rules,
			int byteSize, int blockSize, String dateBirth, String dateChange,
			Boolean hide, int [] blocks) {
		super();
		this.rules = rules;
		this.hide = hide;
		this.blocks = blocks;
	}
	
	public Inode() {
		
	}

	public List<FileProtection> getRules() {
		return rules;
	}

	public void setRules(List<FileProtection> rules) {
		this.rules = rules;
	}
	
	public void addRole(int id, boolean r, boolean w, boolean x) {
		FileProtection newRole = new FileProtection(id, r, w, x);
		this.rules.add(newRole);
	}
	public void addRole(FileProtection newRole) {
		this.rules.add(newRole);
	}
	
	public FileProtection getRights(int id) { // verifica se tem direitos se tiver os retorna
		for (Iterator role = this.rules.iterator(); role.hasNext();) {
			FileProtection type = (FileProtection) role.next();
			if (id == type.getIdRole()) 
				return type;
		}
		return null;
	}

	public Boolean getHide() {
		return hide;
	}

	public void setHide(Boolean hide) {
		this.hide = hide;
	}
	
	public int[] getBlocks() {
		return blocks;
	}

	public void setBlocks(int[] blocks) {
		this.blocks = blocks;
	}

	public Inode getNextBlock() {
		return nextBlock;
	}

	public void setNextBlock(Inode nextBlock) {
		this.nextBlock = nextBlock;
	}

	public void useNextSpace(int blockIndex) {
		Inode aux = this;
		int i = 0;
		while (true) {
			if (i == 8) {
				if (aux.getNextBlock() == null) {
					aux.setNextBlock(new Inode());					
				}
				aux = aux.getNextBlock();
				i = 0;
			}
			if (aux.blocks[i] == -1) {
				aux.blocks[i] = blockIndex;
				break;
			}
			i++;			
			
		}
		
	}
}
