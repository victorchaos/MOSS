package mars.mips.moss.filemanager;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class Directory extends File{

	private File myFather;
	@Expose
	private List<File> files;

	public Directory(int mySelf, File myFather, Inode inode, List<File> files,
			int[] idNodes) {
		super();
		this.myFather = myFather;
		this.files = files;
	}

	public Directory(String name) {
		setName(name);
		files = new ArrayList<File>();
	}
	
	public Directory(String name, Directory father) {
		setName(name);
		this.myFather = father;
		files = new ArrayList<File>();
	}

	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}

	public File getMyFather() {
		return myFather;
	}

	public void setMyFather(File myFather) {
		this.myFather = myFather;
	}	
	
	public boolean add(File file) {		
		return getFiles().add(file);
	}

	public File hasFile(String name) {
		for (File file : getFiles()) {
			if (file.getName().equals(name))
				return file;
		}
		return null;
	}

	public File getFile(int fd) {
		File found = null;
		for (File file : getFiles()) {
			if (file instanceof Directory)
				found = ((Directory)file).getFile(fd);
			else if (file.getDescritor() == fd)
				return file;
			if (found != null)
				return found;
		}
		return found;
	}

	public void loadJsTree(File file) {
		for (File fil : ((Directory)file).getFiles()) {
			fil.setTreeNode(FileManagerController.treePanel.addObject(file.getTreeNode(), fil.getName()));
			if (fil instanceof Directory)
				loadJsTree(fil);
			else
				Spacer.loadInodes(fil);
		}
		
	}
}
