package mars.mips.moss.filemanager;

import javax.swing.tree.DefaultMutableTreeNode;

import com.google.gson.annotations.Expose;

public class File {
	@Expose
	private int idOwner; // id do dono
	@Expose
	private String name; // nome do arquivo
	@Expose
	private Inode inode;
	@Expose
	private String path;	
	private DefaultMutableTreeNode treeNode;
	@Expose
	private int descritor;
	@Expose
	private String dateBirth; // data de criacao
	@Expose
	private String dateChange; // data de modificacao
	@Expose
	private int aloccatedWords;
	private int mode;

	public File(Inode inode) {
		super();
		this.inode = inode;
	}

	public File() {

	}
	
	public int getIdOwner() {
		return idOwner;
	}

	public void setIdOwner(int idOwner) {
		this.idOwner = idOwner;
	}

	public int getAloccatedWords() {
		return aloccatedWords;
	}

	public void setAloccatedWords(int aloccatedWords) {
		this.aloccatedWords = aloccatedWords;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public String getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(String dateBirth) {
		this.dateBirth = dateBirth;
	}

	public String getDateChange() {
		return dateChange;
	}

	public void setDateChange(String dateChange) {
		this.dateChange = dateChange;
	}

	public Inode getInode() {
		return inode;
	}

	public void setInode(Inode inode) {
		this.inode = inode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getDescritor() {
		return descritor;
	}

	public void setDescritor(int descritor) {
		this.descritor = descritor;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public DefaultMutableTreeNode getTreeNode() {
		return treeNode;
	}

	public void setTreeNode(DefaultMutableTreeNode treeNode) {
		this.treeNode = treeNode;
	}
}
