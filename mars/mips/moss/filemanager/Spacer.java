package mars.mips.moss.filemanager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Spacer { // nosso gerenciador de espaco�o livre	

	public Spacer() {
		
	}
	
	public static boolean alloc(int lenght, File file) {
		// criar o inode e alocar no file		
		if (file.getInode() == null) {
			file.setInode(new Inode());
		}
		Inode aux = file.getInode();
		
		int alocWords = lenght/SuperBlock.getWordSize();
		for (int i = 0; i < alocWords; i++) {
			aux.useNextSpace(SuperBlock.putNextWord(file));
			FileManagerController.blockUse++;
		}
		file.setAloccatedWords(file.getAloccatedWords()+alocWords);
		file.setDateChange(new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss").format(Calendar.getInstance().getTime()));
		FileSystem.saveFiles();
		return true;		
	}	
	
	public static void unalloc(int lenght) {
		
	}

	public static void loadInodes(File fil) {
		Inode aux = fil.getInode();		
		for (int i = 0; i < fil.getAloccatedWords(); i++) {			
			SuperBlock.allocWordAt(fil.getDescritor(), aux.getBlocks()[i%8]);
			FileManagerController.blockUse++;
			if (i%8 == 7)
				aux = aux.getNextBlock();
		}
	}	
}
