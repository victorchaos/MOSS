
.include "macros.asm"

.data
file1: .asciiz "a/b/c/arquivo2.txt" #com diretorio, precisa existir
#file1: .asciiz "arquivo1.txt" # sem diret�rio escreve na raiz do projeto
body1: .asciiz "The quick brown fox jumps over the lazy dog" # tem 44 caracteres
file2: .asciiz "a/b/c/arquivo2.txt"
file3: .asciiz "a/b/arquivo2.txt"
output: .asciiz ""
openOk: .asciiz "Arquivo aberto\n"
closeOk: .asciiz "Arquivo fechado\n"
writeOk: .asciiz "Escrita conclu�da\n"
readOk: .asciiz "Leitura conclu�da\n"
warnO: .asciiz "Falha na abertura\n"
warnW: .asciiz "Falha na escrita\n"
warnR: .asciiz "Falha na leitura\n"

.text

#cria��o dos processos
	fork(Programa1, Programa2, 8, 0, 15)
	fork(Programa2, FimPrograma2, 10, 0, 15)		

#esclonando o primeiro processo	
	processChange


Programa1:
	
openFile(file1, 1, 0)
bge $v0,0, rightOpen # se for negativo deu falha
prinThis(warnO)
j END
rightOpen: add $s0,$v0,$zero # salvando file description
prinThis(openOk)
writeFile(body1,44)
bge $v0,0, rightWrite # se for negativo deu falha
prinThis(warnW)
j END
rightWrite: prinThis(writeOk)
closeFile
prinThis(closeOk)

openFile(file1, 1, 0)
bge $v0,0, rightOpen11 # se for negativo deu falha
prinThis(warnO)
j END
rightOpen11: add $s0,$v0,$zero # salvando file description
prinThis(openOk)
writeFile(body1,44)
bge $v0,0, rightWrite11 # se for negativo deu falha
prinThis(warnW)
j END
rightWrite11: prinThis(writeOk)
closeFile
prinThis(closeOk)

END:
processTerminate

Programa2:

openFile(file2, 1, 0)
bge $v0,0, rightOpen2 # se for negativo deu falha
prinThis(warnO)
j END2
rightOpen2: add $s0,$v0,$zero # salvando file description
prinThis(openOk)
writeFile(body1,44)
bge $v0,0, rightWrite # se for negativo deu falha
prinThis(warnW)
j END2
rightWrite2: prinThis(writeOk)
closeFile
prinThis(closeOk)


END2:
processTerminate
FimPrograma2:
