#macro para exit
.macro done
	li $v0,10
	syscall
.end_macro

#macro para imprimir string
.macro prinThis(%str)
    li $v0, 4
    la $a0, %str #lembrar de la
    syscall
.end_macro

#macro para abrir arquivo - (arquivo, flags - 1 write - 0 read, modo de abertura) 
.macro openFile(%file,%flag,%mode)
    li $v0, 13
    la $a0, %file
    li $a1, %flag
    li $a2, %mode
    syscall
.end_macro

#macro para fechar arquivo
.macro closeFile()
    li $v0, 16
    add $a0,$s0, $zero # file descriptor devera estar salvo em $s0
    syscall
.end_macro

#macro para ler arquivo - (arquivo, end. do buffer de entrada, tamanho da leitura) 
.macro readFile(%buffer,%max)
    li $v0, 14
    add $a0,$s0,$zero # file descriptor devera estar salvo em $s0
    la $a1, %buffer
    li $a2, %max
    syscall
.end_macro

#macro para ler arquivo - (end. do buffer de saida, tamanho da escrita) 
.macro writeFile(%buffer,%max)
    li $v0, 15
    add $a0,$s0,$zero #file descriptor devera estar salvo em $s0
    la $a1, %buffer
    li $a2, %max
    syscall
.end_macro

#macro para fork
.macro fork (%labelStart, %labelEnd, %priority, %minPriority, %maxPriority) 
    addi $sp, $sp, -24 
    # empilhando
    sw $a0, 20($sp)   
    sw $a1, 16($sp)   
    sw $a2, 12($sp)   
    sw $a3, 8($sp)   
    sw $v1, 4($sp)
    sw $t7, 0($sp)    

	li $v0, 18	
	li $t7, 1
	la $a0, %labelStart
	la $a1, %labelEnd
	la $v1, %priority
	la $a3, %minPriority
	la $a2, %maxPriority
	syscall
	
    # desempilhando          
    lw $t7, 0($sp)   
    lw $v1, 4($sp) 
    lw $a3, 8($sp)      
    lw $a2, 12($sp)  
    lw $a1, 16($sp)    
    lw $a0, 20($sp)
    
    addi $sp, $sp, 24
.end_macro

#macro para processChange
.macro processChange	
	li $t7, 2
	li $v0,18
	syscall
.end_macro

#macro para processTerminate
.macro processTerminate
	li $t7, 3
	li $v0,18
	syscall
.end_macro

.macro writing(%buffer,%max,%js,%jf)
    writeFile(%buffer,%max)
    bge $v0,0, %js # se for negativo deu falha
    j %jf
.end_macro

.macro reading(%output,%max,%js,%jf)
    readFile(%output,%max)
    bge $v0,0, %js # se for negativo deu falha
    j %jf
.end_macro

.macro opening(%file,%flag,%js,%jf)
    openFile(%file,%flag, 0)
    bge $v0,0, %js # se for negativo deu falha
    j %jf
.end_macro

.macro closing()
    closeFile
.end_macro
