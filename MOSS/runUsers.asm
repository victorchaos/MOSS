.include "macros.asm"
.data
# Saida
output1: .space 1024
output2: .space 1024
output3: .space 1024

# Definição de arquivos
file1: .asciiz "dir1/arquivo11.txt" 
file2: .asciiz "dir2/arquivo21.txt" 
file3: .asciiz "dir3/arquivo31.txt"

# body writing
body1: .asciiz "The quick brown fox jumps over the lazy dog " # tem 44 caracteres
body2: .asciiz "The slowly red fox was taken by the lazy dog" # tem 44 caracteres
body3: .asciiz "The quick brown dog knocked the lazy red fox" # tem 44 caracteres

.text
#criação dos processos
	fork(Programa1, Programa2, 8, 0, 15)
	fork(Programa2, Programa3, 10, 0, 15)
	fork(Programa3, FimPrograma3, 12, 0, 15)	
#escalonando o primeiro processo	
	processChange
	
Programa1: # escreve no dir1/arquivo11.txt
	opening(file1,9,rightOpen11,fimAll1) # abrindo aqruivo para escrita
rightOpen11:
	add $s0,$v0,$zero # salvando file description
	writing(body1,44,rightWrite11,fimAll1) # escrevendo no arquivo
rightWrite11:
	closing() # fechando arquivo
	opening(file1,0,rightOpenR1,fimAll1) # abrindo aqruivo para leitura
rightOpenR1:
	add $s0,$v0,$zero # salvando file description
	reading(output1,44,rightRead1,fimAll1) # lendo do arquivo
rightRead1:
	prinThis(output1) # exibindo arquivo lido
fimAll1:
	closing() # fechando arquivo
	processTerminate # finalizando o processo
Programa2: # escreve no dir2/arquivo21.txt
	opening(file2,9,rightOpen21,fimAll2) # abrindo aqruivo para escrita
rightOpen21:
	add $s0,$v0,$zero # salvando file description
	writing(body2,44,rightWrite21,fimAll2) # escrevendo no arquivo
rightWrite21:
	closing() # fechando arquivo
	opening(file2,0,rightOpenR2,fimAll2) # abrindo aqruivo para leitura
rightOpenR2:
	add $s0,$v0,$zero # salvando file description
	reading(output2,44,rightRead2,fimAll2) # lendo do arquivo
rightRead2:
	prinThis(output2) # exibindo arquivo lido
fimAll2:
	closing() # fechando arquivo
	processTerminate # finalizando o processo
Programa3: # escreve no dir3/arquivo31.txt
	opening(file3,9,rightOpen31,fimAll3) # abrindo aqruivo para escrita
rightOpen31:
	add $s0,$v0,$zero # salvando file description
	writing(body3,44,rightWrite31,fimAll3) # escrevendo no arquivo
rightWrite31:
	closing() # fechando arquivo
	opening(file3,0,rightOpenR3,fimAll3)  # abrindo aqruivo para leitura
rightOpenR3:
	add $s0,$v0,$zero # salvando file description
	reading(output3,44,rightRead3,fimAll3) # lendo do arquivo
rightRead3:
	prinThis(output3) # exibindo arquivo lido
fimAll3:
	closing() # fechando arquivo
	processTerminate # finalizando o processo
FimPrograma3:
